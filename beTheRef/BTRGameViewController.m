//
//  BTRGameViewController.m
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "BTRGameViewController.h"
#import "BTRManagerScoreCollectionViewCell.h"
#import "BTRFeedTableViewCell.h"
#import "DataModel.h"
#import <TSMessages/TSMessage.h>
#import <AFNetworking/AFNetworking.h>

@interface BTRGameViewController (){
    NSMutableArray * matchData;
    NSMutableArray * myPlayers;
    NSMutableArray * otherPlayers;
    NSArray * myImages;
    NSArray * othersImages;
    BOOL myView;
}

@end

@implementation BTRGameViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    myView = TRUE;
    
    myImages = @[@"daviddegea@2x.jpg", @"chelsea-branislav-ivanovic@2x.jpg", @"martinskrtel@2x.jpg", @"philjones@2x.jpg", @"roberthuth@2x.jpg",@"davidnugent@2x.jpg", @"gerrard@2x.jpg",@"alexsong@2x.jpg",  @"aaronramsey@2x.jpg",@"sandro@2x.jpg", @"edenhazard@2x.jpg", @"OlivierGeroud@2x.jpg"];
    
    othersImages = @[@"joehart@2x.jpg", @"chelsea-branislav-ivanovic_2@2x.jpg", @"colman@2x.jpg",@"ZoumanaBakayogo@2x.jpg" ,@"alex_bruce@2x.jpg", @"MorganSchneiderlin@2x.jpg", @"gerrard2@2x.jpg", @"NacerChadli@2x.jpg", @"silva@2x.jpg",@"HarryKane@2x.jpg", @"wilifred@2x.jpg"];
    
    [self fillImagesWithArray:myImages];
    
    matchData = [[NSMutableArray alloc] init];
    myPlayers = [[NSMutableArray alloc] initWithArray:@[@12332, @73374, @73328, @152902, @15522, @61235, @2524, @25737, @63293, @87501, @61611, @80837]];
    otherPlayers = [[NSMutableArray alloc] initWithArray:@[@41742, @73374, @89275, @69162, @34051, @61143, @2524, @76317,@35635, @95495, @83875]];

    self.tableView.estimatedRowHeight = 60.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // Do any additional setup after loading the view.
    
    self.client = [PTPusher pusherWithKey:@"c300e6dc941a80accbf9" delegate:self];
    [self.client connect];
    
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:@"eventstream"];
    [channel bindToEventNamed:@"event" handleWithBlock:^(PTPusherEvent *channelEvent) {

       // NSLog(@"events: %@", channelEvent.data);
        NSError *error;
        BTREvent * event = [MTLJSONAdapter modelOfClass:BTREvent.class fromJSONDictionary:channelEvent.data error:&error];
        [matchData insertObject:event atIndex:0];
        [self calculateScoreWithEvent:event];
        [self.tableView reloadData];
    }];
    
    
    PTPusher * otherPusher = [PTPusher pusherWithKey:@"c300e6dc941a80accbf9" delegate:self];
  //  otherPusher.
  //  otherPusher.authorizationURL = [NSURL URLWithString:@"http://10.100.84.72:5000/pusher/auth"];
    [otherPusher connect];
    
    PTPusherChannel * cardsChannel = [otherPusher subscribeToChannelNamed:@"mychannel"];

    [cardsChannel bindToEventNamed:@"event" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
     /*   [TSMessage showNotificationWithTitle:@"RED CARD!"
                                    subtitle:@"Your friend Adam has given you a RED CARD!"
                                        type:TSMessageNotificationTypeError];
       */
        NSLog(@"RED CARDS: %@", channelEvent.data);
        NSError *error;
        BTREvent * event = [MTLJSONAdapter modelOfClass:BTREvent.class fromJSONDictionary:channelEvent.data error:&error];
        [matchData insertObject:event atIndex:0];
        [self calculateScoreWithEvent:event];
        [self.tableView reloadData];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myPointsChanged:) name:@"myPointsChanged" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(otherPointsChanged:) name:@"otherPointsChanged" object:nil];

}

-(void)pusher:(PTPusher *)pusher didSubscribeToChannel:(PTPusherChannel *)channel{
    NSLog(@"channel: %@", channel.name);
}

-(void)pusher:(PTPusher *)pusher didFailToSubscribeToChannel:(PTPusherChannel *)channel withError:(NSError *)error{
    NSLog(@"Error: %@", error);
}

- (void)pusher:(PTPusher *)pusher willAuthorizeChannel:(PTPusherChannel *)channel withRequest:(NSMutableURLRequest *)request
{
    [request setValue:@"some-authentication-token" forHTTPHeaderField:@"X-MyCustom-AuthTokenHeader"];
}

-(void) fillImagesWithArray:(NSArray*) imageArray{
    
    [self.gkButton setImage:[UIImage imageNamed:[imageArray objectAtIndex:0]] forState:UIControlStateNormal];
    [self.df1Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:1]] forState:UIControlStateNormal];
    [self.df2Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:2]] forState:UIControlStateNormal];
    [self.df3Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:3]] forState:UIControlStateNormal];
    [self.df4Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:4]] forState:UIControlStateNormal];

    [self.midf1Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:5]] forState:UIControlStateNormal];
    [self.midf2Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:6]] forState:UIControlStateNormal];
    [self.midf3Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:7]] forState:UIControlStateNormal];
    [self.midf4Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:8]] forState:UIControlStateNormal];
    
    [self.fw1Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:9]] forState:UIControlStateNormal];
    [self.fw2Button setImage:[UIImage imageNamed:[imageArray objectAtIndex:10]] forState:UIControlStateNormal];

}

-(void) addColorCard:(UIColor*)_color ToIndex:(NSInteger) index{
   
    switch (index) {
        case 0:
            [self.gkBookImage setHidden:NO];
            [self.gkBookImage setBackgroundColor:_color];
            break;
        case 1:
            [self.df1BookImage setHidden:NO];
            [self.df1BookImage setBackgroundColor:_color];
            break;
        case 2:
            [self.df2BookImage setHidden:NO];
            [self.df2BookImage setBackgroundColor:_color];
            break;
        case 3:
            [self.df3BookImage setHidden:NO];
            [self.df3BookImage setBackgroundColor:_color];
            break;
        case 4:
            [self.df4BookImage setHidden:NO];
            [self.df4BookImage setBackgroundColor:_color];
            break;
        case 5:
            [self.midf1BookImage setHidden:NO];
            [self.midf1BookImage setBackgroundColor:_color];
            break;
        case 6:
            [self.midf2BookImage setHidden:NO];
            [self.midf2BookImage setBackgroundColor:_color];
            break;
        case 7:
            [self.midf3BookImage setHidden:NO];
            [self.midf3BookImage setBackgroundColor:_color];
            break;
        case 8:
            [self.midf4BookImage setHidden:NO];
            [self.midf4BookImage setBackgroundColor:_color];
            break;
        case 9:
            [self.fw1BookImage setHidden:NO];
            [self.fw1BookImage setBackgroundColor:_color];
            break;
        case 10:
            [self.fw2BookImage setHidden:NO];
            [self.fw2BookImage setBackgroundColor:_color];
            break;
  
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)myPointsChanged:(NSNotification *)notification {
    NSLog(@"myPoints: %ld", (long)[[DataModel sharedInstance] myScore]);
    [self.collectionView reloadData];
}

- (void)otherPointsChanged:(NSNotification *)notification {
    NSLog(@"othersPoints: %ld", (long)[[DataModel sharedInstance] otherScore]);
    [self.collectionView reloadData];
}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UICollectionView Data Source

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 2;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    BTRManagerScoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if ([[DataModel sharedInstance]myScore] < [[DataModel sharedInstance]otherScore]) {
        if (indexPath.row == 1) {
            cell.managerName.text = @"You";
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", [[DataModel sharedInstance]myScore]];
            cell.positionLabel.text = [NSString stringWithFormat:@"%ld.", (long)indexPath.row+1];
        }
        else{
            cell.backgroundColor = [UIColor whiteColor];
            cell.managerName.text = @"Adam";
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", [[DataModel sharedInstance]otherScore]];
            cell.positionLabel.text = [NSString stringWithFormat:@"%ld.", (long)indexPath.row+1];
            
        }
    }
    else{
        if (indexPath.row == 1) {
            cell.backgroundColor = [UIColor whiteColor];
            cell.managerName.text = @"Adam";
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", [[DataModel sharedInstance]otherScore]];
            cell.positionLabel.text = [NSString stringWithFormat:@"%ld.", (long)indexPath.row+1];

        }
        else{
            cell.managerName.text = @"You";
            cell.scoreLabel.text = [NSString stringWithFormat:@"%ld", [[DataModel sharedInstance]myScore]];
            cell.positionLabel.text = [NSString stringWithFormat:@"%ld.", (long)indexPath.row+1];
            
        }
    }

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
 
    BTRManagerScoreCollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    if ([cell.managerName.text isEqualToString:@"Adam"]) {
        [self fillImagesWithArray:othersImages];
        myView = FALSE;
        self.navigationController.navigationBar.topItem.title = @"Adam's team";
    }
    else{
        [self fillImagesWithArray:myImages];
        myView = TRUE;
        self.navigationController.navigationBar.topItem.title = @"Your team";
    }
    
}

#pragma mark <UICollectionViewDelegateFlowLayout>

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(126, 105);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


#pragma mark - UITableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return matchData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    BTRFeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedCell"];
    
    if (cell == nil) {
        cell = [[BTRFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"feedCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    BTREvent * event =  [matchData objectAtIndex:indexPath.row];
    BTRTime  *time = event.time;
    
   /* if ([event.eventID isEqual:@13]) {
        cell.feedLabel.text = [NSString stringWithFormat:@"Half Over"];
    }
    else if ([event.eventID isEqual:@21]){
        cell.feedLabel.text = [NSString stringWithFormat:@"Start Half!"];
    }
    else{*/
        cell.feedLabel.text = [NSString stringWithFormat:@"Half:%@ Time: %@:%@\n%@",time.half, time.minutes, time.seconds, event.eventDescription];
    //}
    
    return cell;
}

-(void) calculateScoreWithEvent:(BTREvent*) event{
    
    
    if ([event.eventID isEqual:@11]) {
        [self changePointsForEvent:event andOfPoints:100 andDefPoints:-40 andAssistPoints:30];
    }
    else if ([event.eventID isEqual:@17]){
        [self changePointsForEvent:event andOfPoints:100 andDefPoints:-50 andAssistPoints:0];

    }
    else if ([event.eventID isEqual:@18]){
        [self changePointsForEvent:event andOfPoints:-50 andDefPoints:100 andAssistPoints:0];

    }
    else if ([event.eventID isEqual:@19]){
        [self changePointsForEvent:event andOfPoints:50 andDefPoints:0 andAssistPoints:10];

    }
    else if ([event.eventID isEqual:@20]){
        [self changePointsForEvent:event andOfPoints:25 andDefPoints:-40 andAssistPoints:10];

    }
    else if ([event.eventID isEqual:@7]){
        
        BTRPlayer * defPlayer = event.defensivePlayer;
        
        if ([myPlayers containsObject:defPlayer] && myView) {
            [self addColorCard:[UIColor redColor] ToIndex:[myPlayers indexOfObject:defPlayer]];
        }
        else if ([otherPlayers containsObject:defPlayer] && !myView) {
            [self addColorCard:[UIColor redColor] ToIndex:[otherPlayers indexOfObject:defPlayer]];
        }
        
        [self changePointsForEvent:event andOfPoints:-100 andDefPoints:0 andAssistPoints:0];

    }
    else if ([event.eventID isEqual:@2]){
        
        BTRPlayer * defPlayer = event.defensivePlayer;
        
        if ([myPlayers containsObject:defPlayer] && myView) {
            [self addColorCard:[UIColor yellowColor] ToIndex:[myPlayers indexOfObject:defPlayer]];
        }
        else if ([otherPlayers containsObject:defPlayer] && !myView) {
            [self addColorCard:[UIColor yellowColor] ToIndex:[otherPlayers indexOfObject:defPlayer]];
        }
        
        [self changePointsForEvent:event andOfPoints:-30 andDefPoints:0 andAssistPoints:0];

    }
    else if ([event.eventID isEqual:@8]){
        [self changePointsForEvent:event andOfPoints:-10 andDefPoints:0 andAssistPoints:0];

    }
    else if ([event.eventID isEqual:@28]){
        [self changePointsForEvent:event andOfPoints:-100 andDefPoints:0 andAssistPoints:0];

    }

}

-(void) changePointsForEvent:(BTREvent*) event andOfPoints:(NSInteger) ofPoints andDefPoints:(NSInteger) defPoints andAssistPoints:(NSInteger) assistPoints{
    
    if (ofPoints != 0) {
        BTRPlayer * ofPlayer = event.offensivePlayer;
        if ([otherPlayers containsObject:ofPlayer.playerID]){
            [[DataModel sharedInstance] changeOtherPoints:ofPoints];
        }
        if ([myPlayers containsObject:ofPlayer.playerID]){
            [[DataModel sharedInstance] changeMyPoints:ofPoints];
        }
    }
    
    if (defPoints != 0) {
        BTRPlayer * defPlayer = event.defensivePlayer;
        if ([myPlayers containsObject:defPlayer.playerID]){
            [[DataModel sharedInstance] changeMyPoints:defPoints];
        }
        if ([otherPlayers containsObject:defPlayer.playerID]) {
            [[DataModel sharedInstance] changeOtherPoints:defPoints];
        }
    }
    
    if (assistPoints != 0) {
        BTRPlayer * asPlayer = event.assistingPlayer;
        if ([myPlayers containsObject:asPlayer.playerID]){
            [[DataModel sharedInstance] changeMyPoints:assistPoints];
        }
        if ([otherPlayers containsObject:asPlayer.playerID]){
            [[DataModel sharedInstance] changeOtherPoints:assistPoints];
        }

    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
