//
//  DataModel.m
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "DataModel.h"

@implementation DataModel

static DataModel *sharedObject;

+ (DataModel*)sharedInstance
{
    static dispatch_once_t once;
    static DataModel *instance;
    dispatch_once(&once, ^{
        instance = [[DataModel alloc] init];
    });
    
    return instance;
}

-(NSInteger) myScore{
    return myScore;
}

-(NSInteger) otherScore{
    return otherScore;
}

-(void) changeMyPoints:(NSInteger)_points{
    myScore += _points;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myPointsChanged" object:self];

}

-(void)changeOtherPoints:(NSInteger)_points{
    otherScore += _points;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"otherPointsChanged" object:self];

}

-(void) start{
    myScore = 0;
    otherScore = 0;
}

@end
