//
//  BTRTime.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "MTLModel.h"
#import <Mantle/Mantle.h>

@interface BTRTime : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic, readonly) NSString *additionalMinutes;
@property (copy, nonatomic, readonly) NSString *half;
@property (copy, nonatomic, readonly) NSString *minutes;
@property (copy, nonatomic, readonly) NSString *seconds;

@end
