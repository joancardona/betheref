//
//  DataModel.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModel : NSObject{
    NSInteger myScore;
    NSInteger otherScore;

}

+ (DataModel*)sharedInstance;
-(void) changeMyPoints:(NSInteger) _points;
-(NSInteger) myScore;
-(void) changeOtherPoints:(NSInteger) _points;
-(NSInteger) otherScore;
-(void) start;

@end
