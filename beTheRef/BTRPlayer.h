//
//  BTRPlayer.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "MTLModel.h"
#import <Mantle/Mantle.h>

@interface BTRPlayer : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic, readonly) NSString *firstName;
@property (copy, nonatomic, readonly) NSString *lastName;
@property (copy, nonatomic, readonly) NSString *playerID;

@end
