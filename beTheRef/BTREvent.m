//
//  BTREvent.m
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "BTREvent.h"

@implementation BTREvent

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"eventID":@"type_id",
             @"eventName":@"name",
             @"time":@"time",
             @"eventDescription":@"description",
             @"defensivePlayer":@"defensive_player",
             @"offensivePlayer":@"offensive_player",
             @"assistingPlayer":@"assisting_player"

             };
    
}


+ (NSValueTransformer *) timeJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:BTRTime.class];
   // return [MTLJSONAdapter dictionaryTransformerWithModelClass:BTRTime.class];
    //return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[TPDInsurance class]];
}

+ (NSValueTransformer *) defensivePlayerJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:BTRPlayer.class];
    // return [MTLJSONAdapter dictionaryTransformerWithModelClass:BTRTime.class];
    //return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[TPDInsurance class]];
}

+ (NSValueTransformer *) offensivePlayerJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:BTRPlayer.class];
    // return [MTLJSONAdapter dictionaryTransformerWithModelClass:BTRTime.class];
    //return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[TPDInsurance class]];
}

+ (NSValueTransformer *) assistingPlayerJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:BTRPlayer.class];
    // return [MTLJSONAdapter dictionaryTransformerWithModelClass:BTRTime.class];
    //return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[TPDInsurance class]];
}

@end
