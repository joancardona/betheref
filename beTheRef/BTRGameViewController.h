//
//  BTRGameViewController.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Pusher/Pusher.h>
#import "BTREvent.h"
#import "BTRTime.h"
#import "BTRPlayer.h"

@interface BTRGameViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource, PTPusherDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) PTPusher * client;

@property (weak, nonatomic) IBOutlet UIButton *gkButton;
@property (weak, nonatomic) IBOutlet UIButton *df1Button;
@property (weak, nonatomic) IBOutlet UIButton *df2Button;
@property (weak, nonatomic) IBOutlet UIButton *df3Button;
@property (weak, nonatomic) IBOutlet UIButton *df4Button;

@property (weak, nonatomic) IBOutlet UIButton *midf1Button;
@property (weak, nonatomic) IBOutlet UIButton *midf2Button;
@property (weak, nonatomic) IBOutlet UIButton *midf3Button;
@property (weak, nonatomic) IBOutlet UIButton *midf4Button;
@property (weak, nonatomic) IBOutlet UIButton *fw1Button;
@property (weak, nonatomic) IBOutlet UIButton *fw2Button;

@property (weak, nonatomic) IBOutlet UIImageView *gkBookImage;
@property (weak, nonatomic) IBOutlet UIImageView *df1BookImage;
@property (weak, nonatomic) IBOutlet UIImageView *df2BookImage;
@property (weak, nonatomic) IBOutlet UIImageView *df3BookImage;
@property (weak, nonatomic) IBOutlet UIImageView *df4BookImage;

@property (weak, nonatomic) IBOutlet UIImageView *midf1BookImage;
@property (weak, nonatomic) IBOutlet UIImageView *midf2BookImage;

@property (weak, nonatomic) IBOutlet UIImageView *midf3BookImage;
@property (weak, nonatomic) IBOutlet UIImageView *midf4BookImage;

@property (weak, nonatomic) IBOutlet UIImageView *fw1BookImage;
@property (weak, nonatomic) IBOutlet UIImageView *fw2BookImage;



@end
