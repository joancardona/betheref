//
//  BTRTime.m
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "BTRTime.h"

@implementation BTRTime


+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"additionalMinutes":@"additional_minutes",
             @"half":@"half",
             @"minutes":@"minutes",
             @"seconds":@"seconds",
             };
    
}

@end
