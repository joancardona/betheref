//
//  BTREvent.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "MTLModel.h"
#import <Mantle/Mantle.h>
#import "BTRTime.h"
#import "BTRPlayer.h"

@interface BTREvent : MTLModel <MTLJSONSerializing>

@property (copy, nonatomic, readonly) NSString *eventID;
@property (copy, nonatomic, readonly) NSString *eventName;
@property (copy, nonatomic, readonly) NSString *eventDescription;
@property (copy, nonatomic, readonly) BTRTime *time;
@property (copy, nonatomic, readonly) BTRPlayer *offensivePlayer;
@property (copy, nonatomic, readonly) BTRPlayer *defensivePlayer;
@property (copy, nonatomic, readonly) BTRPlayer *assistingPlayer;

@end
