//
//  ViewController.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BTRFriendTableViewCell.h"
#import <MZTimerLabel/MZTimerLabel.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *addFriendButton;
@property (weak, nonatomic) IBOutlet UIButton *pickAtTeamButton;
@property (weak, nonatomic) IBOutlet UIButton *choseBetButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MZTimerLabel *countDown;

@end

