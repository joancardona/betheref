//
//  ViewController.m
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    self.addFriendButton.layer.cornerRadius = 10; // this value vary as per your desire
    self.addFriendButton.clipsToBounds = YES;
    
    self.pickAtTeamButton.layer.cornerRadius = 10; // this value vary as per your desire
    self.pickAtTeamButton.clipsToBounds = YES;
    
    self.choseBetButton.layer.cornerRadius = 10; // this value vary as per your desire
    self.choseBetButton.clipsToBounds = YES;
    
    [self countdown];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BTRFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[BTRFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

 
-(void)countdown{
 
    NSString *dateString =  @"2015-04-18T17:00:00.000Z";
    NSDateFormatter *startdateFormatter = [[NSDateFormatter alloc] init];
 // this is imporant - we set our input date format to match our input string
 // if format doesn't match you'll get nil from your string, so be careful
    [startdateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSDate *startingDate = [[NSDate alloc] init];
 // voila!
    startingDate = [startdateFormatter dateFromString:dateString];
 
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
 //  NSDate *startingDate = [dateFormatter dateFromString:@"2005-01-01"];
    NSDate *endingDate = [NSDate date];
 
 
    NSTimeInterval doubleDiff = [startingDate timeIntervalSinceDate:endingDate];
    long diff = (long) doubleDiff;
    int seconds = diff % 60;
    diff = diff / 60;
    int minutes = diff % 60;
    diff = diff / 60;
    int hours = diff % 24;
    int days = diff / 24;
 
 
    self.countDown.text = [NSString stringWithFormat:@"%d days %d hours %d min %d sec", days, hours, minutes, seconds];
 
    [self performSelector:@selector(countdown) withObject:nil afterDelay:1];
 
 }

 



@end
