//
//  BTRManagerScoreCollectionViewCell.h
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTRManagerScoreCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *managerName;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;

@end
