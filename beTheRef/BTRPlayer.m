//
//  BTRPlayer.m
//  beTheRef
//
//  Created by Joan on 18/4/15.
//  Copyright (c) 2015 Joan Cardona. All rights reserved.
//

#import "BTRPlayer.h"

@implementation BTRPlayer

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"firstName":@"first_name",
             @"lastName":@"last_name",
             @"playerID":@"id"
             };
}


@end
